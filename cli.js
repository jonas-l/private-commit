#!/usr/bin/env node

const { spawn } = require('child_process')

function getRoundedDate() {
  let date = new Date()

  // round to day
  date.setUTCMilliseconds(0)
  date.setUTCSeconds(0)
  date.setUTCMinutes(0)
  date.setUTCHours(0)

  // round down to previous start of week
  let dayOfWeek = date.getDay()	// Sunday to Saturday = 0 to 6
  dayOfWeek-- // Sunday to Saturday = -1 to 5
  if (dayOfWeek == -1) dayOfWeek = 6	// Monday to Sunday = 1 to 6

  return new Date(date.valueOf() - 1000 * 60 * 60 * 24 * dayOfWeek)
}

const date = getRoundedDate().toUTCString()

console.log('pcommit command is deprecated, plese use a more specific version')

setTimeout(() => {
  spawn('git', ['commit', ...process.argv.slice(2)], {
    env: {
      GIT_AUTHOR_DATE: date,
      GIT_COMMITTER_DATE: date,
      ...process.env
    },
    stdio: 'inherit'
  })
}, 1000 * 3)
