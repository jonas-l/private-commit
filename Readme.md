## private-commit

When you use git and make a commit, then the timestamp of the commit is saved.
However, you eventually work at public git repositories and don't want a public protocol of your working times.
This is the solution.

This small application rounds the current date
down to the start of the last week/ hour/ day, sets ``GIT_AUTHOR_DATE`` and ``GIT_COMMITTER_DATE`` accordingly and runs git commit.

### Installation

There are 2 ways for 2 different versions.
Only use one of these two.

### Install as shell alias

Install ``faketime`` and add the following to your ``~/.bashrc``:

```
phcommit() {
  local T="$(LC_ALL=en_US.utf8 date -u "+%a, %d %b %Y %H:00:00 %Z")"
  local T="$(LC_ALL=en_US.utf8 date -d"$T" "+%Y-%m-%d %H:00:00")"
  faketime -f "$T" git commit $*
}

pdcommit() {
  local T="$(LC_ALL=en_US.utf8 date -u "+%a, %d %b %Y 00:00:00 %Z")"
  local T="$(LC_ALL=en_US.utf8 date -d"$T" "+%Y-%m-%d %H:00:00")"
  faketime -f "$T" git commit $*
}

pwcommit() {
  local T="$(LC_ALL=en_US.utf8 date -u -dlast-monday "+%a, %d %b %Y %H:%M:00 %Z")"
  local T="$(LC_ALL=en_US.utf8 date -d"$T" "+%Y-%m-%d %H:00:00")"
  faketime -f "$T" git commit $*
}
```

### Install using NPM

Clone <https://codeberg.org/jonas-l/private-commit.git> using git and run ``npm link`` within it.

### Usage

Use ``p[x]commit`` instead of ``git commit`` to make commits. Command line parameters are passed through, so you can use
``p[x]commit -a`` and it will execute ``git commit -a``.

Note: Replace the x
- by ``ẁ`` for rounding to the previous monday
- by ``d`` for rounding to the day
- by ``h`` for rounding to the hour

Warning: There is still some logging due to the logged push events.
