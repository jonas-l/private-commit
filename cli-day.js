#!/usr/bin/env node

const { spawn } = require('child_process')

function getRoundedDate() {
  let date = new Date()

  // round to day
  date.setUTCMilliseconds(0)
  date.setUTCSeconds(0)
  date.setUTCMinutes(0)
  date.setUTCHours(0)

  return date
}

const date = getRoundedDate().toUTCString()

spawn('git', ['commit', ...process.argv.slice(2)], {
  env: {
    GIT_AUTHOR_DATE: date,
    GIT_COMMITTER_DATE: date,
    ...process.env
  },
  stdio: 'inherit'
})
